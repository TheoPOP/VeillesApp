angular.module('starter.controllers', [])

.controller('VeillesCtrl', function($scope, VeillesFactory) {
  $scope.veilles = [];
  VeillesFactory.getAll().then(function(r) {
    $scope.veilles = r.data.veilles;
      console.log($scope.veilles);
  });
  $scope.veilleSelectionne = null;

  $scope.selectionVeille = function(veille) {
    $scope.veilleSelectionne = veille;
  }
})

.controller('UsersCtrl', function($scope, UsersFactory) {
  $scope.users = [];
  UsersFactory.getAll().then(function(r) {
    $scope.users = r.data.users;
      console.log($scope.users);
  });
})
