angular.module('starter.services', [])

.factory('VeillesFactory', function($http){
  return {
    getAll:function(){
      return $http.get('http://veille.popschool.fr/'+'api/?api=veille'+'&action=getAll');
  }
}
})

.factory('UsersFactory', function($http){
  return {
    getAll:function(){
      return $http.get('http://veille.popschool.fr/'+'api/?api=user'+'&action=getAll');
  }
}
});
